package game;

import component.GameComponent;
import objects.*;
import characters.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.*;
import java.util.function.BiFunction;

import javax.swing.JPanel;

import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private static final ResourceBundle INTRO_LABELS = ResourceBundle.getBundle("resources.Initialization");

    private Image imageBackground1;
    private Image imageBackground2;
    private Image imageCastle;
    private Image imageStart;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private List<GameObject> objects = new ArrayList<>();
    private List<ObjectInterchangeable> interchangeablesObject = new ArrayList<>();
    private List<DeadlyCharacter> characters = new ArrayList<>();

    private final JumpingCharacter mario;

    private int nOfMushroom;
    private int nOfTurtle;
    private int nOfTunnel;
    private int nOfBlock;
    private int nOfPiece;

    private Image imgFlag;
    private Image imgCastle;

    public Platform() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imageBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imageBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imageCastle = Utils.getImage(Res.IMG_CASTLE);
        this.imageStart = Utils.getImage(Res.START_ICON);

        mario = Mario.getMario(300, 245);

        this.nOfMushroom = this.howMany("mush");
        this.nOfTurtle = this.howMany("turt");
        this.nOfTunnel = this.howMany("tunn");
        this.nOfBlock = this.howMany("bloc");
        this.nOfPiece = this.howMany("piec");

        addToList(nOfMushroom, "mushroom", characters, Mushroom::new);
        addToList(nOfTurtle, "turtle", characters, Turtle::new);
        addToList(nOfTunnel, "tunnel", objects, Tunnel::new);
        addToList(nOfBlock, "block", objects, Block::new);
        addToList(nOfPiece, "piece", interchangeablesObject, Piece::new);

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    private static <T>  void addToList(final int nOfComponents, final String componentType, final List<? super T> list, BiFunction<Integer, Integer, T> constructor) {
        for(int i = 1; i <= nOfComponents; i++) {
            list.add(constructor.apply(Integer.parseInt(INTRO_LABELS.getString(componentType+i+"x")), Integer.parseInt(INTRO_LABELS.getString(componentType+i+"y"))));
        }
    }

    private static int howMany(final String typeOfComponent) {
        return (int)Collections.list(INTRO_LABELS.getKeys()).stream().filter(x -> x.startsWith(typeOfComponent)).count() / 2;
    }

    public JumpingCharacter getMario() {
        return this.mario;
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    private void areNear(final GameCharacter character, final GameComponent component) {
        if (character.isCloseToAComponent(component)) {
            character.contactWithComponent(component);
        }
    }

    private void lookToAllCharacters(final int start1, final int stop1, final int start2, final int stop2, final GameComponent component) {
        for(int j = start1; j < stop1; j++) {
            this.areNear(this.characters.get(j),component);
            for(int k = start2; k < stop2; k++) {
                this.areNear(this.characters.get(j),this.characters.get(k));
            }
            this.areNear(this.mario,this.characters.get(j));
        }
    }

    public void paintComponent(final Graphics graphic) {
        super.paintComponent(graphic);
        Graphics graphic2D = (Graphics2D) graphic;

        for (int i = 0; i < objects.size(); i++) {
            this.areNear(this.mario,this.objects.get(i));

            this.lookToAllCharacters(0, this.nOfMushroom, this.nOfMushroom, this.nOfMushroom + this.nOfTurtle, this.objects.get(i));

            this.lookToAllCharacters(this.nOfMushroom, this.nOfMushroom + this.nOfTurtle, 0, this.nOfMushroom, this.objects.get(i));
        }

        for (int i = 0; i < interchangeablesObject.size(); i++) {
            if (this.mario.isInContactWithANInterchangeableObject(this.interchangeablesObject.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.interchangeablesObject.remove(i);
            }
        }

        // Moving fixed gameObjects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= 4600) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).shift();
            }

            for (int i = 0; i < interchangeablesObject.size(); i++) {
                this.interchangeablesObject.get(i).shift();
            }

            for(int i = 0; i < this.nOfMushroom; i++) {
                this.characters.get(i).shift();
            }
            for(int i = this.nOfMushroom; i < this.nOfMushroom + this.nOfTurtle; i++) {
                this.characters.get(i).shift();
            }
        }

        graphic2D.drawImage(this.imageBackground1, this.background1PosX, 0, null);
        graphic2D.drawImage(this.imageBackground2, this.background2PosX, 0, null);
        graphic2D.drawImage(this.imageCastle, 10 - this.xPos, 95, null);
        graphic2D.drawImage(this.imageStart, 220 - this.xPos, 234, null);

        for (int i = 0; i < objects.size(); i++) {
            graphic2D.drawImage(this.objects.get(i).getImage(), this.objects.get(i).getX(),
                    this.objects.get(i).getY(), null);
        }

        for (int i = 0; i < interchangeablesObject.size(); i++) {
            graphic2D.drawImage(this.interchangeablesObject.get(i).changeImage(), this.interchangeablesObject.get(i).getX(),
                    this.interchangeablesObject.get(i).getY(), null);
        }

        graphic2D.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        graphic2D.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario.isJumping()) {
            graphic2D.drawImage(this.mario.jump(), this.mario.getX(), this.mario.getY(), null);
        } else {
            graphic2D.drawImage(this.mario.changeImage(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.getX(), this.mario.getY(), null);
        }
        for(int i = 0; i < this.nOfMushroom; i++) {
            if (this.characters.get(i).isAlive()) {
                graphic2D.drawImage(this.characters.get(i).changeImage(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), this.characters.get(i).getX(), this.characters.get(i).getY(), null);
            } else {
                graphic2D.drawImage(this.characters.get(i).changeImageAfterDeath(), this.characters.get(i).getX(), this.characters.get(i).getY() + MUSHROOM_DEAD_OFFSET_Y, null);
                this.characters.get(i).setMoving(false);
            }
        }
        for(int i = this.nOfMushroom; i < this.nOfMushroom + this.nOfTurtle; i++) {
            if (this.characters.get(i).isAlive()) {
                graphic2D.drawImage(this.characters.get(i).changeImage(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), this.characters.get(i).getX(), this.characters.get(i).getY(), null);
            } else {
                graphic2D.drawImage(this.characters.get(i).changeImageAfterDeath(), this.characters.get(i).getX(), this.characters.get(i).getY() + TURTLE_DEAD_OFFSET_Y, null);
                this.characters.get(i).setMoving(false);
            }
        }
    }
}
