package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.platform.getMario().isAlive() == true) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.platform.getxPos() == -1) {
                    Main.platform.setxPos(0);
                    Main.platform.setBackground1PosX(-50);
                    Main.platform.setBackground2PosX(750);
                }
                Main.platform.getMario().setMoving(true);
                Main.platform.getMario().setMovingRight(true);
                Main.platform.setMov(1);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.platform.getxPos() == 4601) {
                    Main.platform.setxPos(4600);
                    Main.platform.setBackground1PosX(-50);
                    Main.platform.setBackground2PosX(750);
                }

                Main.platform.getMario().setMoving(true);
                Main.platform.getMario().setMovingRight(false);
                Main.platform.setMov(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.platform.getMario().setJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.platform.getMario().setMoving(false);
        Main.platform.setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
