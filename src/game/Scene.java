package game;

import utils.Res;
import utils.Utils;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JPanel;

public class Scene extends JPanel {

    public static final int BACKGROUND_OFFSET_X = -50;
    public static final int BACKGROUND_OFFSET_Y = 0;
    public static final int MARIO_OFFSET_X = 300;
    public static final int MARIO_OFFSET_Y = 245;
    private Image imgBackground;
    private Image imgMario;

    public Scene() {
        super();
        this.imgBackground = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
    }

    @Override
    protected void paintComponent(final Graphics graphic) {
        super.paintComponent(graphic);
        Graphics graphic2D = (Graphics2D) graphic;
        graphic2D.drawImage(this.imgBackground, BACKGROUND_OFFSET_X, BACKGROUND_OFFSET_Y, null);
        graphic2D.drawImage(imgMario, MARIO_OFFSET_X, MARIO_OFFSET_Y, null);
    }
}
