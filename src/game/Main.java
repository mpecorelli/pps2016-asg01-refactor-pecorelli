package game;

import javax.swing.JFrame;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";

    public static Platform platform;

    public static void main(String[] args) {
        JFrame finestra = new JFrame(WINDOW_TITLE);
        finestra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        finestra.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        finestra.setLocationRelativeTo(null);
        finestra.setResizable(true);
        finestra.setAlwaysOnTop(true);

        platform = new Platform();
        finestra.setContentPane(platform);
        finestra.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

}
