package game;

public class Refresh implements Runnable {

    private final int PAUSE = 3;

    public void run() {
        while (true) {
            Main.platform.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

} 
