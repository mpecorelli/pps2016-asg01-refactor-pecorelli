package game;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Audio {
    private Clip clip;

    public Audio(final String son) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(son));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            // TODO: log error
        }
    }

    public Clip getClip() {
        return clip;
    }

    public void play() {
        clip.start();
    }

    public void stop() {
        clip.stop();
    }

    public static void playSound(final String son) {
        Audio s = new Audio(son);
        s.play();
    }
}
