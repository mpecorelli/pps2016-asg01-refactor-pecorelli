package characters;

import java.awt.Image;

import component.Component;
import component.GameComponent;
import objects.GameObject;
import objects.ObjectInterchangeable;
import utils.Res;
import utils.Utils;

public abstract class Character extends Component implements GameCharacter {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int MOVING_MARGIN = 5;

    private boolean isMoving;
    private boolean isMovingRight;
    private int counter;
    private boolean isAlive;

    public Character(final int x, final int y, final int width, final int height){
        super(x, y, width, height);

        this.counter = 0;
        this.isMoving = false ;
        this.isMovingRight = false ;
        this.isAlive = true;
    }

    @Override
    public void setAlive(final boolean isAlive) {
        this.isAlive = isAlive;
    }

    @Override
    public void setMoving(final boolean isMoving) {
        this.isMoving = isMoving;
    }

    @Override
    public void setMovingRight(final boolean isMovingRight) {
        this.isMovingRight = isMovingRight;
    }
    @Override
    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public boolean isMovingRight() {
        return this.isMovingRight;
    }

    @Override
    public Image changeImage(final String name, final int frequency){
        String path = Res.IMG_BASE + name + (!this.isMoving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.isMovingRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(path);
    }

    public abstract void contactWithComponent(final GameComponent component);

    public boolean isInContactWithANInterchangeableObject(final ObjectInterchangeable objectInterchangeable) {
        return (isMovingLeft(objectInterchangeable) || isMovingUp(objectInterchangeable) || isMovingRight(objectInterchangeable) || isMovingDown(objectInterchangeable));
    }

    @Override
    public boolean isCloseToAComponent(final GameComponent component){
        return ((this.getX() > component.getX() - PROXIMITY_MARGIN && this.getX()< component.getX() + component.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX()+ this.getWidth() > component.getX() - PROXIMITY_MARGIN && this.getX() + this.getWidth() < component.getX() + component.getWidth() + PROXIMITY_MARGIN));
    }

    protected boolean isMovingRight(final GameComponent component){
        if ((component instanceof Character) && !this.isMovingRight()){
            return false;
        }
        return !(this.getX() + this.getWidth() < component.getX() || this.getX() + this.getWidth() > component.getX() + MOVING_MARGIN ||
                this.getY() + this.getHeight() <= component.getY() || this.getY() >= component.getY() + component.getHeight());
    }

    protected boolean isMovingLeft(final GameComponent component){
        return !(this.getX() > component.getX() + component.getWidth() ||this.getX() + this.getWidth() < component.getX() + component.getWidth() - MOVING_MARGIN ||
                this.getY() + this.getHeight() <= component.getY() || this.getY() >= component.getY() + component.getHeight());
    }

    protected boolean isMovingDown(final GameComponent component){
        int i = 0;
        if(component instanceof GameObject) {
            i = MOVING_MARGIN;
        }
        return !(this.getX() + this.getWidth() < component.getX() + i || this.getX()  > component.getX() + component.getWidth() - i ||
                this.getY() + this.getHeight() < component.getY() || this.getY() +this.getHeight() > component.getY() + i);
    }

    protected boolean isMovingUp(final GameComponent component){
        return !(this.getX() + this.getWidth() < component.getX() + MOVING_MARGIN || this.getX()  > component.getX() + component.getWidth() - MOVING_MARGIN ||
                this.getY() < component.getY() + component.getHeight() || this.getY() > component.getY()+ component.getHeight() + MOVING_MARGIN);
    }

}
