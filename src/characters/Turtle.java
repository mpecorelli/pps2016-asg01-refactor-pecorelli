package characters;

import java.awt.Image;
import utils.Res;
import utils.Utils;

public class Turtle extends DeadlyCharacter {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Turtle(final int x, final int y) {
        super(x, y, WIDTH, HEIGHT, Utils.getImage(Res.IMG_TURTLE_IDLE));

        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    public Image changeImageAfterDeath(){
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
