package characters;

import java.awt.Image;

import component.GameComponent;
import game.Main;
import utils.Res;
import utils.Utils;

public class Mario extends JumpingCharacter {

    private static Mario MARIO = null;

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;

    private Image imgMario;
    private int jumpingExtent;

    private Mario(final int x, final int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumpingExtent = 0;
    }

    public static synchronized Mario getMario(final int x, final int y) {
        return MARIO == null ? MARIO = new Mario(x,y) : MARIO;
    }

    public Image getImage() {
        return this.imgMario;
    }

    public Image jump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Main.platform.getHeightLimit())
                this.setY(this.getY() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;

            str = isMovingRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < Main.platform.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = isMovingRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = isMovingRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            setJumping(false);
            this.jumpingExtent = 0;
        }

        return Utils.getImage(str);
    }

    public void contactWithComponent(final GameComponent component) {
        if(component instanceof Character) {
            Character character = (Character) component;
            if (isMovingRight(character) || isMovingLeft(character)) {
                if (character.isAlive()) {
                    setMoving(false);
                    setAlive(false);
                } else {
                    setAlive(true);
                }
            } else if (isMovingDown(character)) {
                character.setMoving(false);
                character.setMovingRight(false);
                character.setAlive(false);
            }
        }
        if (isMovingRight(component) && isMovingRight() || isMovingLeft(component) && !isMovingRight()) {
            Main.platform.setMov(0);
            this.setMoving(false);
        }

        if (isMovingDown(component) && isJumping()) {
            Main.platform.setFloorOffsetY(component.getY());
        } else if (!isMovingDown(component)) {
            Main.platform.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!isJumping()) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (isMovingUp(component)) {
                Main.platform.setHeightLimit(component.getY() + component.getHeight());
            } else if (!isMovingUp(component) && !isJumping()) {
                Main.platform.setHeightLimit(0);
            }
        }

    }

}
