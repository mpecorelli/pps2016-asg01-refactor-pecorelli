package characters;

import java.awt.*;

/**
 * Created by margherita on 16/03/17.
 */
public abstract class JumpingCharacter extends Character  {

    private boolean isJumping;

    public JumpingCharacter(int x, int y, int width, int height) {
        super(x, y, width, height);
        this.isJumping = false;
    }

    public boolean isJumping() {
        return this.isJumping;
    }

    public void setJumping(final boolean isJumping) {
        this.isJumping = isJumping;
    }

    public abstract Image jump();

}
