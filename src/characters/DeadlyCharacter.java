package characters;

import component.GameComponent;
import java.awt.*;

/**
 * Created by margherita on 15/03/17.
 */
public abstract class DeadlyCharacter extends Character implements Runnable {

    private Image image;

    private final int PAUSE = 15;

    public DeadlyCharacter(final int x, final int y, final int width, final int heigt, final Image image) {
        super(x, y, width, heigt);
        super.setMovingRight(true);
        super.setMoving(true);
        this.image = image;
    }

    @Override
    public void setMoving(final boolean isMoving) {
        super.setMoving(isMoving);
        if(!isMoving) {
            super.setMovingRight(false);
        }
    }

    @Override
    public void shift(){
        setX(getX() + (isMovingRight() ? 1 : -1));
    }

    @Override
    public void run() {
        while (true) {
            if (isAlive()) {
                this.shift();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contactWithComponent(final GameComponent component){
        if(isMovingRight(component) && isMovingRight()){
            setMovingRight(false);
        } else if (isMovingLeft(component) && !isMovingRight()){
            setMovingRight(true);
        }
    }

    public abstract Image changeImageAfterDeath();
}
