package characters;

import component.GameComponent;
import objects.ObjectInterchangeable;

import java.awt.Image;

public interface GameCharacter extends GameComponent{

	void setAlive(boolean isAlive);

	void setMoving(boolean isMoving);

	void setMovingRight(boolean isMovingRight);

	boolean isAlive();

	boolean isMovingRight();

	Image changeImage(String name, int frequency);

	boolean isCloseToAComponent(GameComponent component);

	void contactWithComponent(GameComponent component);

	boolean isInContactWithANInterchangeableObject(ObjectInterchangeable objectInterchangeable);

}
