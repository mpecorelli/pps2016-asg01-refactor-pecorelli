package characters;

import java.awt.Image;
import utils.Res;
import utils.Utils;

public class Mushroom extends DeadlyCharacter {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    public Mushroom(final int x, final int y) {
        super(x, y, WIDTH, HEIGHT, Utils.getImage(Res.IMG_MUSHROOM_DEFAULT));

        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
    }

    public Image changeImageAfterDeath(){
        return Utils.getImage(this.isMovingRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}
