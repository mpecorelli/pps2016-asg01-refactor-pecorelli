package component;

/**
 * Created by margherita on 15/03/17.
 */
public interface GameComponent {

    int getWidth();

    int getHeight();

    int getX();

    int getY();

    void setX(int x);

    void setY(int y);

    void shift();

}
