package objects;

import component.Component;
import java.awt.*;

/**
 * Created by margherita on 15/03/17.
 */
public class GameObjectImpl extends Component implements GameObject{

    private Image image;

    public GameObjectImpl(final int x, final int y, final int width, final int height, final Image image){
        super(x, y, width, height);

        this.image = image;
    }

    @Override
    public Image getImage() {
        return this.image;
    }

    @Override
    public void setImage(final Image image) {
        this.image = image;
    }

}
