package objects;

import java.awt.Image;

import component.GameComponent;

public interface GameObject extends GameComponent{

    Image getImage();

    void setImage(Image image);

}
