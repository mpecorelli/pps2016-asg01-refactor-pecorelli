package objects;

import utils.Res;
import utils.Utils;
import java.awt.Image;

public class Piece extends GameObjectImpl implements Runnable, ObjectInterchangeable{

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 100;
    private int counter;


    public Piece(final int x, final int y) {
        super(x, y, WIDTH, HEIGHT, Utils.getImage(Res.IMG_PIECE1));
    }

    @Override
    public Image changeImage() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.changeImage();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

}
