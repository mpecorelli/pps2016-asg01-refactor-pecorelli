package objects;

import java.awt.*;

/**
 * Created by margherita on 15/03/17.
 */
public interface ObjectInterchangeable extends GameObject {

    Image changeImage();

}
